Pair Styles sw/angle/table and threebody/table
==============================================

This repository contains the LAMMPS pair styles sw/angle/table (Stillinger-Weber pair style with tabulated angular interactions)
and threebody/table (fully tabulated three-body forces) within two separate folders: sw_angle_table and threebody_table.

Both pair styles are included in the MANYBODY package of the LAMMPS_ release from the 23rd of June, 2022 (Download_).
The respository contains them in the format they are within this release.

Both pair styles have been renamed when been included into the LAMMPS stable release.
In our publications, the pair style sw/angle/table previously has been called sw/table and the
pair style threebody/table previously has been called 3b/table.
Please heave this in mind when using them.

Each folder contains the subfolders *MANYBODY** with the source (.cpp) and header (.h) files (as in the LAMMPS release from the 23rd of June),
*doc* with documentation in ReStructuredText format (as in the LAMMPS release from the 23rd of June) and *tutorial* with tutorials.

The documentation is also available under (https://docs.lammps.org/pair_sw_angle_table.html, pair sw/angle/table) and
(https://docs.lammps.org/pair_threebody_table.html, pair threebody/table).

To use the pair styles, you have to:

1. Download the respective lammps version

2. Run ``make yes-manybody`` in $LAMMPSROOT/src/ to include the manybody package

3. Compile LAMMPS


LAMMPS download links:

.. _LAMMPS: https://lammps.sandia.gov/
.. _Download: https://download.lammps.org/tars/lammps-23Jun2022.tar.gz


If you make use of the pair style sw/angle/table, please cite:

** C. Scherer, and D. Andrienko, Understanding three-body contributions to coarse-grained force fields, Phys. Chem. Chem. Phys, 20(34):22387–22394, 2018, http://xlink.rsc.org/?DOI=C8CP00746B

If you make use of the pair style threebody/table, please cite:

** C. Scherer, R. Scheid, D. Andrienko, and T. Bereau, Kernel-Based Machine Learning for Efficient Simulations of Molecular Liquids, J. Chem. Theor. Comp., 16(5):3194–3204, 2020, https://doi.org/10.1021/acs.jctc.9b01256


Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

::

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

