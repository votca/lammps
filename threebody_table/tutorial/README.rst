Tutorial for pair style threebody/table
=======================================

This tutorial contains all required input files for the simulation of CG SPC/E water with
the user pair style threebody/table, as well as a run.sh script.

Running the simulations, you will reproduce results of the publication :ref:`Scherer2 <Scherer2>`
where a water molecule is represented by one coarse-grained (CG) bead. The tutorial contains
two parts. The three-body (force) tables for both parts (``1-1-1.table`` and ``1-1-2.table``) have been parametrized with the kernel-based
machine learning (ML) with the VOTCA package (<https://gitlab.mpcdf.mpg.de/votca/votca>). For a general description of the table format
have a look at the documentation of this pair style. For a tutorial on the parametrization, have a look at
<https://gitlab.mpcdf.mpg.de/votca/votca/-/tree/master/csg-tutorials/guide> and
<https://gitlab.mpcdf.mpg.de/votca/votca/-/tree/master/csg-tutorials/ml>. In both cases, the parametrization has been
done according to the three-body forces of the FM tabulated Stillinger-Weber (sw/table) potential with the covariant meshing technique with the
settings files used in <https://gitlab.mpcdf.mpg.de/votca/votca/-/tree/master/csg-tutorials/ml/3body/with_binning>.

For the first part of tutorial, the folder contains the contains the LAMMPS data file (``spce.data``) with the starting configuration
of 1000 CG water molecules, an input file (``spce.in``) and a three-body file (``spce.3b``).

The lammps input file contains the lines specifying the pair style and coefficients:

- ``pair_style      hybrid/overlay table linear 1200 threebody/table`` - use a combination of pair\_style table with 1200 linear table entries and the pair style threebody/table
- ``pair_coeff      1 1 table table_CG_CG.txt VOTCA`` - set the table name and keyword for the pair\_style table
- ``pair_coeff      * * threebody/table spce.3b type`` - set the name of 3body file and bead type for the pair\_style threebody/table

A hybrid pair style is used, where pair forces are calculated as a tabulated interaction (``table_CG_CG.txt``) and the
pair style threebody/table is only used to calculate the three-body forces. The tabulated pair interaction is the same as in the tutorial of
the sw/angle/table pair style (https://gitlab.mpcdf.mpg.de/votca/lammps/-/tree/master/sw_angle_table/tutorial).

To run the simulation, one needs an additional 3body file (``spce.3b``).
It has the following structure:

- ``type`` - relates to keyword `type` in LAMMPS input file
- ``type`` - relates to keyword `type` in LAMMPS input file
- ``type`` - relates to keyword `type` in LAMMPS input file
- ``3.7`` - cutoff in Ang
- ``1-1-1.table`` - name of 3-body force table
- ``ENTRY1`` - keyword in 3-body force table for this interaction
- ``linear`` - interpolation is linear
- ``12`` - number of grid points in radial direction (automatically sets grid size, in this case to 1872)

As there is only one atom type (``1``), the force table is symmetric and contains "M = N * N * (N+1)"
(12 * 12 * 13 = 1872) entries.

The LAMMPS simulation is a standard nvt simulation. A dump file is output with the positions and forces every 10 time steps.
You can calculate the pair distribution and compare it to the ones in the publications
:ref:`Scherer1 <Scherer1>` and :ref:`Scherer2 <Scherer2>`.

For the second part of the tutorial, have a look at the LAMMPS data file (``spce_2.data``),
the input file (``spce_2.in``) and the three-body file (``spce_2.3b``). Running the second part, you will in fact perform the
same MD simulation as in the first part of the tutorial. However, the atom type of the first 100 CG water molecules
has been changed from ``1`` to ``2``. This is done to demonstrate how to run a simulation with different atom types.

Again, lammps input file (``spce_2.in``) contains the lines specifying the pair style and coefficients:

- ``pair_style      hybrid/overlay table linear 1200 threebody/table`` - use a combination of pair\_style table with 1200 linear table entries and the pair style threebody/table
- ``pair_coeff      1 1 table table_CG_CG.txt VOTCA`` - set the table name and keyword for the pair\_style table
- ``pair_coeff      * * threebody/table spce_2.3b type1 type2`` - set the name of 3body file and bead type for the pair\_style threebody/table

Now, the atom type ``1`` is mapped to the element ``type1`` and the atom type ``2`` is mapped to the element ``type2`` in the 3body file (``spce_2.3b``).
For this (artificial) two-element simulation, the 3body file now contain 8 entries for: type1 type1 type1, type1 type1 type2,
type1 type2 type1, type1 type2 type2, type2 type1 type1, type2 type1 type2, type2 type2 type1, type2 type2 type2.
Each entry has the same structure as above. However, entries where the second and the third element are different require
a different force table (``1-1-2.table``) instead of (``1-1-1.table``). ``1-1-2.table`` contains exactly the
force constants as ``1-1-1.table``. However it has to have the asymmetric structure where both interparticle distances
(r_ij and r_ik) are varied from rmin to rmax and therefore contains "M = 2 * N * N * N" (2 * 12 * 12 * 12 = 3456) entries.

Now run the simulation. The theromodynamic output, as well as, the pair correlation function should be exactly the same as for
the first part of the tutorial.


-----------

.. _Scherer1:

**(Scherer1)** C. Scherer, and D. Andrienko, Understanding three-body contributions to coarse-grained force fields, Phys. Chem. Chem. Phys, 20(34):22387–22394, 2018, http://xlink.rsc.org/?DOI=C8CP00746B

.. _Scherer2:

**(Scherer2)** C. Scherer, R. Scheid, D. Andrienko, and T. Bereau, Kernel-Based Machine Learning for Efficient Simulations of Molecular Liquids, J. Chem. Theor. Comp., 16(5):3194–3204, 2020, https://doi.org/10.1021/acs.jctc.9b01256
